# Results

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     6.14ms    1.89ms  13.91ms   77.68%
    Req/Sec     5.40k     1.46k    6.66k    65.11%
  1450642 requests in 45.00s, 298.82MB read
Requests/sec:  32233.60
Transfer/sec:      6.64MB
```