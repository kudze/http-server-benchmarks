# Results

## Without docker (openswoole)

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    12.51ms    1.56ms  92.57ms   95.64%
    Req/Sec     2.65k   108.31     4.36k    91.30%
  713321 requests in 45.01s, 126.53MB read
Requests/sec:  15849.79
Transfer/sec:      2.81MB
```

## Without docker (swoole)

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    12.93ms    1.32ms  76.88ms   93.93%
    Req/Sec     2.57k   123.44     3.23k    86.00%
  689906 requests in 45.01s, 140.80MB read
Requests/sec:  15328.27
Transfer/sec:      3.13MB
```

## Inside docker (openswoole)

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    18.51ms    1.53ms  88.40ms   94.97%
    Req/Sec     1.79k    68.99     2.66k    79.70%
  481710 requests in 45.01s, 85.45MB read
Requests/sec:  10703.26
Transfer/sec:      1.90MB
```

## Inside docker (swoole)

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    18.85ms    1.90ms  79.16ms   91.06%
    Req/Sec     1.76k   110.18     2.38k    81.22%
  473035 requests in 45.02s, 96.54MB read
Requests/sec:  10507.01
Transfer/sec:      2.14MB
```