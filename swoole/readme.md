# Results

## Without docker container

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.64ms  325.98us   6.92ms   91.21%
    Req/Sec    20.16k     2.74k   39.13k    77.56%
  5416717 requests in 45.02s, 852.35MB read
Requests/sec: 120319.56
Transfer/sec:     18.93MB
```

## Without docker container (streams on)

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.51ms  158.20us   4.54ms   87.08%
    Req/Sec    21.88k     1.77k   68.19k    82.94%
  5882465 requests in 45.10s, 0.90GB read
Requests/sec: 130431.29
Transfer/sec:     20.52MB
```

## Inside docker

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.17ms  212.07us  12.89ms   86.38%
    Req/Sec     6.42k   201.31     6.67k    85.07%
  1724414 requests in 45.01s, 271.35MB read
Requests/sec:  38308.94
Transfer/sec:      6.03MB
```