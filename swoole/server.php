<?php

use Swoole\Http\Server;
use Swoole\Http\Request;
use Swoole\Http\Response;

$server = new Server("0.0.0.0", 80);

$server->on("Start", function (Server $server) {
    echo "Swoole http server is started at http://127.0.0.1:80\n";
});

$server->on("Request", function (Request $request, Response $response) {
    $response->end("Hello World\n");
});

$server->start();