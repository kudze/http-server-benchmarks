# Results

## Without docker (symfony serve)

```
Running 45s test @ http://127.0.0.1:8000/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    61.18ms    7.19ms 186.00ms   92.66%
    Req/Sec   541.72     63.80   666.00     77.33%
  145670 requests in 45.04s, 25.70MB read
Requests/sec:   3234.49
Transfer/sec:    584.36KB
```

## Inside docker (httpd + fpm)

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   607.69ms   79.79ms   1.81s    96.58%
    Req/Sec    60.35     46.49   262.00     72.62%
  14445 requests in 45.03s, 3.24MB read
  Socket errors: connect 0, read 2, write 0, timeout 48
Requests/sec:    320.75
Transfer/sec:     73.61KB
```

## Inside docker (nginx + fpm)

```
Running 45s test @ http://127.0.0.1/
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   615.41ms   65.12ms   1.20s    95.64%
    Req/Sec    66.42     59.44   282.00     76.46%
  14395 requests in 45.05s, 3.45MB read
Requests/sec:    319.55
Transfer/sec:     78.33KB
```