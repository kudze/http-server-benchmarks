<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HelloWorldController
{
    #[Route('/')]
    public function helloWorld(): Response
    {
        return new Response("Hello World!");
    }
}