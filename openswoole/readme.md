# Results

## On host machine

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   496.58us    1.43ms  76.81ms   99.65%
    Req/Sec    65.13k    10.51k  102.72k    71.26%
  17498114 requests in 45.01s, 2.23GB read
Requests/sec: 388750.09
Transfer/sec:     50.79MB
```

## In docker

TODO: why slower?

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   790.40us    1.37ms  73.39ms   98.80%
    Req/Sec    42.19k     2.01k   63.20k    79.04%
  11335594 requests in 45.01s, 1.45GB read
Requests/sec: 251861.58
Transfer/sec:     32.91MB
```