# Results

## Without docker:

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     4.84ms   15.93ms 542.87ms   99.48%
    Req/Sec     8.51k   604.77    12.65k    80.29%
  2286826 requests in 45.00s, 333.68MB read
Requests/sec:  50815.61
Transfer/sec:      7.41MB
```

## With docker:

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.18ms   17.51ms 589.47ms   99.43%
    Req/Sec     8.04k   589.68    10.07k    82.33%
  2158181 requests in 45.01s, 314.90MB read
Requests/sec:  47953.04
Transfer/sec:      7.00MB
```