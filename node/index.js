import http from "http";

http.createServer((request, response) => {
    response.writeHead(200);
    response.write('Hello World!');
    response.end();
}).listen(80);