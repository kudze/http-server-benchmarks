# Benchmark of various HTTP web servers.

## Description of a test

The hello world application is hosted in various tools via Docker. 
Then tested by [wrk](https://github.com/wg/wrk).

Command to run tests:

```
wrk -t6 -c200 -d45s http://127.0.0.1/
```

## Important notes

* When testing performance `-d` flag was passed to `docker compose up` command so that the http server logs 
  would not be subscribed to from host machine (Speeding up performance).
* To minimize Docker networking's overhead `host` networking mode is in use.

## Unresolved Q

* Why OpenSwoole is much faster than Swoole? (Seeing 3x more performance??).