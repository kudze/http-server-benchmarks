# Results

## Without docker

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.27ms  841.37us  54.21ms   98.14%
    Req/Sec    14.60k   467.84    18.08k    96.30%
  3923246 requests in 45.00s, 546.26MB read
Requests/sec:  87180.71
Transfer/sec:     12.14MB
```

## Inside docker

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.23ms  453.06us  33.72ms   96.03%
    Req/Sec    14.75k     0.85k   45.23k    99.52%
  3966657 requests in 45.10s, 552.30MB read
Requests/sec:  87952.82
Transfer/sec:     12.25MB
```