# Results

```
Running 45s test @ http://127.0.0.1/hello.php
  6 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    10.64ms   30.99ms 505.66ms   98.04%
    Req/Sec     4.77k   521.02    14.39k    92.38%
  1282514 requests in 45.10s, 244.78MB read
  Socket errors: connect 0, read 12705, write 0, timeout 0
Requests/sec:  28437.10
Transfer/sec:      5.43MB
```